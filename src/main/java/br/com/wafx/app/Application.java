package br.com.wafx.app;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.wafx.v2com.model.Media;

/**
 * @author Kaio Maximiano
 */
@OpenAPIDefinition(info = @Info(description = "Endpoints V2COM", title = "V2COM", version = "0.0.1"))
public class Application extends javax.ws.rs.core.Application {
}