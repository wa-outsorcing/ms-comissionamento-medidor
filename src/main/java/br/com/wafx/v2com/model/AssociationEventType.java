package br.com.wafx.v2com.model;
/**
 * @author Kaio Maximiano
 */
public enum AssociationEventType {

	ASSOCIATE(0), UPDATE(1), DISSOCIATE(2);

	public int value;

	AssociationEventType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
