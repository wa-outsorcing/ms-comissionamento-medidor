package br.com.wafx.v2com.model;
/**
 * @author Kaio Maximiano
 */
public class CommissioningInstallationFieldEquipment {

	private String key;
	private AssociationEventType eventType;
	private String fieldEquipmentKey;
	private FieldEquipment fieldEquipment;
	private String communicationDeviceKey;
	private CommunicationDevice communicationDevice;
	private String mediaKey;
	private Media media;
	private Integer tcpPort;
	private Boolean keepPortOpen;
	private String channelUri;
	private Integer idleTimeout;
	private String version;
	private Integer timestamp;

	public CommissioningInstallationFieldEquipment() {
	}

	public CommissioningInstallationFieldEquipment(String key, AssociationEventType eventType, String fieldEquipmentKey,
			FieldEquipment fieldEquipment, String communicationDeviceKey, CommunicationDevice communicationDevice,
			String mediaKey, Media media, Integer tcpPort, Boolean keepPortOpen, String channelUri, Integer idleTimeout,
			String version, Integer timestamp) {
		this.key = key;
		this.eventType = eventType;
		this.fieldEquipmentKey = fieldEquipmentKey;
		this.fieldEquipment = fieldEquipment;
		this.communicationDeviceKey = communicationDeviceKey;
		this.communicationDevice = communicationDevice;
		this.mediaKey = mediaKey;
		this.media = media;
		this.tcpPort = tcpPort;
		this.keepPortOpen = keepPortOpen;
		this.channelUri = channelUri;
		this.idleTimeout = idleTimeout;
		this.version = version;
		this.timestamp = timestamp;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public AssociationEventType getEventType() {
		return eventType;
	}

	public void setEventType(AssociationEventType eventType) {
		this.eventType = eventType;
	}

	public String getFieldEquipmentKey() {
		return fieldEquipmentKey;
	}

	public void setFieldEquipmentKey(String fieldEquipmentKey) {
		this.fieldEquipmentKey = fieldEquipmentKey;
	}

	public FieldEquipment getFieldEquipment() {
		return fieldEquipment;
	}

	public void setFieldEquipment(FieldEquipment fieldEquipment) {
		this.fieldEquipment = fieldEquipment;
	}

	public String getCommunicationDeviceKey() {
		return communicationDeviceKey;
	}

	public void setCommunicationDeviceKey(String communicationDeviceKey) {
		this.communicationDeviceKey = communicationDeviceKey;
	}

	public CommunicationDevice getCommunicationDevice() {
		return communicationDevice;
	}

	public void setCommunicationDevice(CommunicationDevice communicationDevice) {
		this.communicationDevice = communicationDevice;
	}

	public String getMediaKey() {
		return mediaKey;
	}

	public void setMediaKey(String mediaKey) {
		this.mediaKey = mediaKey;
	}

	public Media getMedia() {
		return media;
	}

	public void setMedia(Media media) {
		this.media = media;
	}

	public Integer getTcpPort() {
		return tcpPort;
	}

	public void setTcpPort(Integer tcpPort) {
		this.tcpPort = tcpPort;
	}

	public Boolean getKeepPortOpen() {
		return keepPortOpen;
	}

	public void setKeepPortOpen(Boolean keepPortOpen) {
		this.keepPortOpen = keepPortOpen;
	}

	public String getChannelUri() {
		return channelUri;
	}

	public void setChannelUri(String channelUri) {
		this.channelUri = channelUri;
	}

	public Integer getIdleTimeout() {
		return idleTimeout;
	}

	public void setIdleTimeout(Integer idleTimeout) {
		this.idleTimeout = idleTimeout;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Integer timestamp) {
		this.timestamp = timestamp;
	}

}
