package br.com.wafx.v2com.model;

/**
 * @author Kaio Maximiano
 */
public class FieldEquipment {
	private String key;
	private LifecycleEventType eventType;
	private String identificationCode;
	private String description;
	private String serialNumber;
	private Boolean installed;
	private String type;
	private String manufacturer;
	private Boolean excluded;
	private String encryptionKey;
	private String version;
	private Integer timestamp;

	public FieldEquipment() {
	}
	
	public FieldEquipment(String key, LifecycleEventType eventType, String identificationCode, String description,
			String serialNumber, Boolean installed, String type, String manufacturer, Boolean excluded,
			String encryptionKey, String version, Integer timestamp) {
		this.key = key;
		this.eventType = eventType;
		this.identificationCode = identificationCode;
		this.description = description;
		this.serialNumber = serialNumber;
		this.installed = installed;
		this.type = type;
		this.manufacturer = manufacturer;
		this.excluded = excluded;
		this.encryptionKey = encryptionKey;
		this.version = version;
		this.timestamp = timestamp;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public LifecycleEventType getEventType() {
		return eventType;
	}

	public void setEventType(LifecycleEventType eventType) {
		this.eventType = eventType;
	}

	public String getIdentificationCode() {
		return identificationCode;
	}

	public void setIdentificationCode(String identificationCode) {
		this.identificationCode = identificationCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Boolean getInstalled() {
		return installed;
	}

	public void setInstalled(Boolean installed) {
		this.installed = installed;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Boolean getExcluded() {
		return excluded;
	}

	public void setExcluded(Boolean excluded) {
		this.excluded = excluded;
	}

	public String getEncryptionKey() {
		return encryptionKey;
	}

	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Integer timestamp) {
		this.timestamp = timestamp;
	}

}
