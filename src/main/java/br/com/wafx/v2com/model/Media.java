package br.com.wafx.v2com.model;

/**
 * @author Kaio Maximiano
 */
public class Media {

	private String key;
	private LifecycleEventType eventType;
	private String identifier;
	private String description;
	private String mediaType;
	private Boolean installed;
	private Boolean excluded;
	private String dataPlanId;
	private String version;
	private Integer timestamp;

	public Media() {
	}

	public Media(String key, LifecycleEventType eventType, String identifier, String description, String mediaType,
			Boolean installed, Boolean excluded, String dataPlanId, String version, Integer timestamp) {
		this.key = key;
		this.eventType = eventType;
		this.identifier = identifier;
		this.description = description;
		this.mediaType = mediaType;
		this.installed = installed;
		this.excluded = excluded;
		this.dataPlanId = dataPlanId;
		this.version = version;
		this.timestamp = timestamp;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public LifecycleEventType getEventType() {
		return eventType;
	}

	public void setEventType(LifecycleEventType eventType) {
		this.eventType = eventType;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public Boolean getInstalled() {
		return installed;
	}

	public void setInstalled(Boolean installed) {
		this.installed = installed;
	}

	public Boolean getExcluded() {
		return excluded;
	}

	public void setExcluded(Boolean excluded) {
		this.excluded = excluded;
	}

	public String getDataPlanId() {
		return dataPlanId;
	}

	public void setDataPlanId(String dataPlanId) {
		this.dataPlanId = dataPlanId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Integer timestamp) {
		this.timestamp = timestamp;
	}

}
