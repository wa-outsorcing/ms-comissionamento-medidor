package br.com.wafx.v2com.model;

/**
 * @author Kaio Maximiano
 */
public enum LifecycleEventType {
	ENTITY_CREATED(0), ENTITY_CHANGED(1), ENTITY_ENABLED(3), ENTITY_EXCLUDED(4);

	public int value;

	LifecycleEventType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
