package br.com.wafx.v2com.resource;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import br.com.wafx.v2com.restClient.RestClientMdmVision;

/**
 * @author Kaio Maximiano
 */
@Path("/v2com")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Resource {

	@Inject
	@RestClient
	RestClientMdmVision restClientMdmVision;

	@GET
	@Path("/installations-cache/installation/{meterId}/{installationCode}")
	@Operation(description = "Endpoint REST para Busca de Dados de Instalação em cache no MDM-Vision", summary = "Endpoint REST para Busca de Dados de Instalação em cache no MDM-Vision")
	public Response findInstallationCache(@PathParam("meterId") String meterId, @PathParam("installationCode") String installationCode) {
		String result = restClientMdmVision.findInstalationCache(meterId, installationCode);
		if (result == null || result.isEmpty())
			Response.status(Status.NOT_FOUND.ordinal());
		return Response.ok(result).build();
	}

	//@PATCH
	@PUT
	@Path("/installations/update-register/{meterId}/{installationCode}")
	@Operation(description = "Endpoint REST para Atualização do Registro de Instalação no MDM-Vision", summary = "Endpoint REST para Atualização do Registro de Instalação no MDM-Vision")
	public Response updateRegisterInstalation(@PathParam("meterId") String meterId, @PathParam("installationCode") String installationCode) {
		return Response.ok(restClientMdmVision.updateRegisterInstalation(meterId, installationCode)).build();
	}

	@POST
	@Path("/commission-installations")
	@Operation(description = "Endpoint REST POST para comissionamento da instalação no MDM-Vision", summary = "Endpoint REST POST para comissionamento da instalação no MDM-Vision")
	public Response commissionInstalation() {
		return Response.ok(restClientMdmVision.commissionInstalation()).build();
	}

	@PUT
	@Path("/commissioning/{meterId}/{installationCode}")
	@Operation(description = "Endpoint REST para Registro de Comissionamento", summary = "Endpoint REST para Registro de Comissionamento")
	public Response commissioningInstalation(@PathParam("meterId") String meterId, @PathParam("installationCode") String installationCode) {
		return Response.ok(restClientMdmVision.commissioningInstalation(meterId, installationCode)).build();
	}

}